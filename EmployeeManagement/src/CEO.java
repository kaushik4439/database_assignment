import java.util.Map.Entry;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class CEO extends Director {

	private static final Scanner SCANNER = new Scanner(System.in);
	private Employee employee = new Employee();
	private MainClass mainClass = new MainClass();
	private JDBC jdbc = new JDBC();

	public CEO(int employeeID, String name, int age) {
		super(employeeID, name, age);

	}

	public CEO() {

	}

	// CEO hiring a new director
	public boolean hireDirector(String department) {

		String name = null;
		int age = 0;

		System.out.println("enter name ");
		name = mainClass.validateNullString(SCANNER.nextLine());
		System.out.println("enter age of director ");
		age = Integer.parseInt(SCANNER.nextLine());
		System.out.println("enter mentor ID ");
		int mentorID = mainClass.validate(SCANNER.nextLine());
		int deptID;
		try {
			String designation = jdbc.getDesignation(mentorID);
			if(!designation.equalsIgnoreCase("ceo")) {
				System.out.println("mentor of director can only be ceo");
				return false;
			}
			deptID = jdbc.getDepartmentID(department);
			if (deptID == 0) {
				System.out.println("enter valid department");
				return false;
			}

			jdbc.addNewEmployee(name, deptID, "director", age, mentorID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

	// CEO promoting a employee
	public boolean promoteEmployee(int employeeID, String newDesignation) {

		try {
			if (!jdbc.checkEmployeeWithID(employeeID)) {
				System.out.println("no employee with this ID");
				return false;
			}
			String designation = jdbc.getDesignation(employeeID);
			if (newDesignation.equalsIgnoreCase(designation)) {
				System.out.println("Already on same post");
				return false;
			}

			jdbc.promoteEmployee(employeeID, newDesignation);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

	// CEO deleting a employee
	public boolean deleteEmployee(int employeeID) {
		try {
			if (!jdbc.checkEmployeeWithID(employeeID)) {
				System.out.println("no employee with this ID");
				return false;
			}
			String designation = jdbc.getDesignation(employeeID);
			if (designation.equalsIgnoreCase("ceo")) {
				System.out.println("Sorry you can't delete this employee");
				return false;
			}
			jdbc.deleteEmployee(employeeID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

}
