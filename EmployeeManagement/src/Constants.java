
public class Constants {
	public static interface Queries {
		
		public static final String INSERT_EMPLOYEE = "INSERT INTO `Employee` ("
				+"  `name`,"
				+"  `created_at`,"
				+"  `updated_at`,"
				+"  `deptID`,"
				+"  `designation`,"
				+"  `age`,"
				+"  `mentor`"
				+") "
				+"VALUES"
				+"  ("
				+"    ?,"
				+"    NOW(),"
				+"    NOW(),"
				+"    ?,"
				+"    ?,"
				+"    ?,"
				+"    ?"
				+"  );";
		public static final String INSERT_DEPARTMENT = "insert into Department(name,created_at,updated_at) values('?',now(),now())";
		public static final String SHOW_EMPLOYEE = "select * from Employee";	
		public static final String GET_DEPARTMENT = "select deptID from Department where name = ?";
		public static final String CHECK_EMPLOYEE = "select * from Employee where empID = ?";
		public static final String GET_DESIGNATION = "select designation from Employee where empID = ?";
		public static final String DELETE_EMPLOYEE = "delete from Employee where empID = ?";
		public static final String PROMOTE_EMPLOYEE = "update Employee set designation = ? where empID = ?";
		public static final String EMPLOYEE_UNDER_MENTOR = "select * from Employee where mentor = ?";
		 
	}

}
