import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Director extends HumanResource {

	private static final Scanner SCANNER = new Scanner(System.in);
	private MainClass mainClass = new MainClass();
	private Employee employee = new Employee();
	private static ArrayList<Employee> list;
	private static JDBC jdbc = new JDBC();

	public Director(int employeeID, String name, int age) {
		super(employeeID, name, age);

	}

	public Director() {

	}

	// director promoting a employee
	public boolean promoteEmployee(int employeeID, String newDesignation) {

		try {
			if (!jdbc.checkEmployeeWithID(employeeID)) {
				System.out.println("no employee with this ID");
				return false;
			}
			String designation = jdbc.getDesignation(employeeID);
			if (newDesignation.equalsIgnoreCase(designation)) {
				System.out.println("Already on same post");
				return false;
			}
			if (designation.equalsIgnoreCase("ceo") || designation.equalsIgnoreCase("director")
					|| newDesignation.equalsIgnoreCase("ceo")) {
				System.out.println("Sorry you don't have access to promote this employee");
				return false;
			}

			jdbc.promoteEmployee(employeeID, newDesignation);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

	// director hiring a new HR
	public boolean hireHR(String department) {

		String name;
		int age;
		
		System.out.println("enter name of HR ");
		name = mainClass.validateNullString(SCANNER.nextLine());
		System.out.println("enter age of HR ");
		age = mainClass.validate(SCANNER.nextLine());
		System.out.println("enter mentor ID ");
		int mentorID = mainClass.validate(SCANNER.nextLine());
		int deptID;
		try {
			String designation = jdbc.getDesignation(mentorID);
			
			if(!designation.equalsIgnoreCase("ceo") && !designation.equalsIgnoreCase("director")) {
				System.out.println("mentor of HR can only be of higher post than HR");
				return false;
			}
			deptID = jdbc.getDepartmentID(department);
			if(deptID == 0) {
				System.out.println("enter valid department");
				return false;
			}
				
			jdbc.addNewEmployee(name, deptID, "hr", age, mentorID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

	// director deleting a employee
	public boolean deleteEmployee(int employeeID) {
		try {
			if (!jdbc.checkEmployeeWithID(employeeID)) {
				System.out.println("no employee with this ID");
				return false;
			}
			String designation = jdbc.getDesignation(employeeID);
			if (designation.equalsIgnoreCase("ceo") || designation.equalsIgnoreCase("director")) {
				System.out.println("Sorry you can't delete this employee");
				return false;
			}
			jdbc.deleteEmployee(employeeID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

}
