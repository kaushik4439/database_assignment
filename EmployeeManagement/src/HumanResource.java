
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class HumanResource extends Manager {

	private static final Scanner SCANNER = new Scanner(System.in);
	private MainClass mainClass = new MainClass();
	JDBC jdbc = new JDBC();

	public HumanResource(int employeeID, String name, int age) {
		super(employeeID, name, age);

	}

	public HumanResource() {

	}

	// HR hiring a new Manager
	public boolean hireManager(String department) {

		String name;
		int age, deptID;

		System.out.println("enter name ");
		name = mainClass.validateNullString(SCANNER.nextLine());
		System.out.println("enter age");
		age = mainClass.validate(SCANNER.nextLine());
		System.out.println("enter mentor ID ");
		int mentorID = mainClass.validate(SCANNER.nextLine());

		try {
			String designation = jdbc.getDesignation(mentorID);
			if (!designation.equalsIgnoreCase("ceo") && !designation.equalsIgnoreCase("director")
					&& !designation.equalsIgnoreCase("HR")) {
				System.out.println("mentor of manager can only be of higher post than manager");
				return false;
			}
			deptID = jdbc.getDepartmentID(department);
			if (deptID == 0) {
				System.out.println("enter valid department");
				return false;
			}

			jdbc.addNewEmployee(name, deptID, "manager", age, mentorID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

	// HR hiring a fresher
	public boolean hireFresher( String designation, String department) {
		String name;
		int age, deptID;

		System.out.println("enter name ");
		name = mainClass.validateNullString(SCANNER.nextLine());
		System.out.println("enter age");
		age = mainClass.validate(SCANNER.nextLine());
		System.out.println("enter mentor ID ");
		int mentorID = mainClass.validate(SCANNER.nextLine());

		try {
			String mentorDesignation = jdbc.getDesignation(mentorID);
			if (!mentorDesignation.equalsIgnoreCase("ceo") && !mentorDesignation.equalsIgnoreCase("director")
					&& !mentorDesignation.equalsIgnoreCase("HR") && !mentorDesignation.equalsIgnoreCase("manager")) {
				System.out.println("mentor of freasher can only be of higher post than freasher");
				return false;
			}
			deptID = jdbc.getDepartmentID(department);
			if (deptID == 0) {
				System.out.println("enter valid department");
				return false;
			}

			if (designation.equalsIgnoreCase("ceo") || designation.equalsIgnoreCase("director")
					|| designation.equalsIgnoreCase("HR") || designation.equalsIgnoreCase("manager")) {
				System.out.println("enter designation of fresher only");
				return false;
			}
			jdbc.addNewEmployee(name, deptID, designation, age, mentorID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;

	}

	// HR promoting a new employee
	public boolean promoteEmployee(int employeeID, String newDesignation) {

		try {
			if (!jdbc.checkEmployeeWithID(employeeID)) {
				System.out.println("no employee with this ID");
				return false;
			}
			String designation = jdbc.getDesignation(employeeID);
			if (newDesignation.equalsIgnoreCase(designation)) {
				System.out.println("Already on same post");
				return false;
			}
			if (designation.equalsIgnoreCase("director") || designation.equalsIgnoreCase("ceo")
					|| designation.equalsIgnoreCase("HR") || newDesignation.equalsIgnoreCase("ceo")
					|| newDesignation.equalsIgnoreCase("director")) {
				System.out.println("Sorry you don't have access to promote this employee");
				return false;
			}

			jdbc.promoteEmployee(employeeID, newDesignation);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

	// HR deleting a employee
	public boolean deleteEmployee(int employeeID) {

		try {
			if (!jdbc.checkEmployeeWithID(employeeID)) {
				System.out.println("no employee with this ID");
				return false;
			}
			String designation = jdbc.getDesignation(employeeID);
			if (designation.equalsIgnoreCase("ceo") || designation.equalsIgnoreCase("director")
					|| designation.equalsIgnoreCase("HR")) {
				System.out.println("Sorry you can't delete this employee");
				return false;
			}
			jdbc.deleteEmployee(employeeID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

}