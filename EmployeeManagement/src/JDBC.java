import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.ResourceBundle;


public class JDBC {

	public static String JDBC_DRIVER ;
	public static String TARGET_DB_URL;
	public static String OUTPUT_FOLDER_PATH ;
	public static String INFORMATION_DB_URL ;
	public static String USER ;
	public static String PASS ;
	static Connection connection = null;
	public static ResourceBundle bundle = null;
	PreparedStatement pstmt = null ;
	ResultSet rs = null ;
	 static {
		 
		 bundle=  ResourceBundle.getBundle("db");
		 JDBC_DRIVER = bundle.getString("JDBC_DRIVER");
		 TARGET_DB_URL = bundle.getString("TARGET_DB_URL");
		 USER = bundle.getString("USER");
		 PASS = bundle.getString("PASS");
		 
		  try{
			    
				connection = getDbTxConnection(TARGET_DB_URL, USER, PASS);
			    }catch(Exception e){
			    	e.printStackTrace();
			    }
	 }
	
	public static Connection getDbTxConnection(String dbUrl, String username, String password){
		 Connection conn = null;
		 try{
		      Class.forName(JDBC_DRIVER);
		      System.out.println("Connecting to database... ");
		      conn = DriverManager.getConnection(dbUrl,username,password);
		      System.out.println("connected");
		     }catch(SQLException se){
		      se.printStackTrace();
		   }catch(Exception e){
		      e.printStackTrace();
		   }
		 return conn;
	}
	public void addNewEmployee(String name, int deptID, String designation,int age, int mentorID) throws SQLException {
		try {

			connection.setAutoCommit(false);
			pstmt = connection.prepareStatement(Constants.Queries.INSERT_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, name);
			pstmt.setInt(2, deptID);
			pstmt.setString(3, designation);
			pstmt.setInt(4,age);
			pstmt.setInt(5, mentorID);
			int numero = pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}  
		}
		connection.commit();
		
	}
	public void showAllEmployee() throws SQLException {
		try {
			//connection.setAutoCommit(false);
			pstmt = connection.prepareStatement(Constants.Queries.SHOW_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			
			rs = pstmt.executeQuery();
			while(rs.next()){
				
				System.out.println(" employee id " + rs.getInt(1)+ "    name "+ rs.getString(2)+"    department "+ rs.getInt(5)+ "    designation " +rs.getString(6));

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			} 
		}
		
		
	}
	public int getDepartmentID(String deptName) throws SQLException {
		
		try {
			
			pstmt = connection.prepareStatement(Constants.Queries.GET_DEPARTMENT, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, deptName);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				
				return rs.getInt(1);
			}
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}  
		}
		return 0;
		
		
	}
	public boolean checkEmployeeWithID(int empID) throws SQLException {
		
		try {
			
			pstmt = connection.prepareStatement(Constants.Queries.CHECK_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, empID);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				
				return true;
			}
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}  
		}
		return false;
		
	}
	public String getDesignation(int empID) throws SQLException {
		
		try {
			
			pstmt = connection.prepareStatement(Constants.Queries.GET_DESIGNATION, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, empID);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				
				return rs.getString(1);
			}
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}  
		}
		return null;
		
	}
	
	public boolean deleteEmployee(int empID) throws SQLException {
		try {
			
			pstmt = connection.prepareStatement(Constants.Queries.DELETE_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, empID);
			int numero = pstmt.executeUpdate();
			
			} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}  
		}
		return true;
		
	}
	public boolean promoteEmployee(int employeeID, String newDesignation) throws SQLException {
		
		try {
			
			pstmt = connection.prepareStatement(Constants.Queries.PROMOTE_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, newDesignation);
			pstmt.setInt(2, employeeID);
			int numero = pstmt.executeUpdate();
			
			} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}  
		}
		return true;
		
		
	}
	
	public boolean getEmployeeUnderMentor(int mentorID) throws SQLException {
		
		try {
			//connection.setAutoCommit(false);
			pstmt = connection.prepareStatement(Constants.Queries.EMPLOYEE_UNDER_MENTOR, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, mentorID);
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				
				System.out.println(" employee id " + rs.getInt(1)+ " name "+ rs.getString(2));

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			} 
		}
		return true;
		
	}
	
}
