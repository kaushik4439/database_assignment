import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class MainClass {

	private static final Scanner SCANNER = new Scanner(System.in);

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub

		MainClass mainClass = new MainClass();
		JDBC jdbc = new JDBC();
		int employeeID, userChoice = 0, mentorID;
		int key = 0;
		jdbc.showAllEmployee();

		
			System.out.println(
					"who you are ...\nenter 1 for ceo \n enter 2 for director \n enter 3 for HR \n enter 4 for manager\n enter 100 to terminate");
			userChoice = mainClass.validate(SCANNER.nextLine());

			String department;

			if (userChoice == 1) {
				while (key != 100) {

					CEO ceo = new CEO();
					System.out.println(
							"enter 1 to hire new employee \n enter 2 to delete\n enter 3 to promote employee \n enter 4 to show employee under you");
					key = mainClass.validate(SCANNER.nextLine());
					if (key == 1) {

						System.out.println("enter department ");
						department = mainClass.validateNullString(SCANNER.nextLine());
						System.out.println("enter 1 to hire director\n enter 2 to hire HR\nenter 3 to hire manager");
						key = mainClass.validate(SCANNER.nextLine());
						System.out.println("enter mentor ID ");
						mentorID = mainClass.validate(SCANNER.nextLine());
						if (key == 1)
							ceo.hireDirector(department);
						else if (key == 2)
							ceo.hireHR(department);
						else if (key == 3)
							ceo.hireManager(department);
						else
							System.out.println("enter valid input");
					} else if (key == 2) {
						System.out.println("enter ID ");
						employeeID = mainClass.validate(SCANNER.nextLine());
						ceo.deleteEmployee(employeeID);
					} else if (key == 3) {
						System.out.println("enter ID ");
						employeeID = mainClass.validate(SCANNER.nextLine());
						System.out.println("enter new designation of employee you want to promote");
						String newDesignation = mainClass.validateNullString(SCANNER.nextLine());
						ceo.promoteEmployee(employeeID, newDesignation);
					}else if (key == 4) {
						System.out.println("enter your emp ID ");
						mentorID = mainClass.validate(SCANNER.nextLine());
						ceo.getEmployeeUnderMentor(mentorID);

					}

					else if (key == 100)
						break;
					else {
						System.out.println("enter valid choice");
						continue;
					}
				}
			}

			else if (userChoice == 2) {
				while (key != 100) {

					Director director = new Director();
					System.out.println(
							"enter 1 to hire new employee \n enter 2 to delete\n enter 3 to promote employee\nenter 4 to show employee under you \nenter 100 to go back");
					key = mainClass.validate(SCANNER.nextLine());
					if (key == 1) {

						
						System.out.println("enter department");
						department = mainClass.validateNullString(SCANNER.nextLine());
						System.out.println("enter 1 to hire HR\n enter 2 to hire manager");
						key = mainClass.validate(SCANNER.nextLine());
						if (key == 1)
							director.hireHR(department);
						else if (key == 2)
							director.hireManager(department);
						else
							System.out.println("enter valid input");

					}

					else if (key == 2) {
						System.out.println("enter ID ");
						employeeID = mainClass.validate(SCANNER.nextLine());
						director.deleteEmployee(employeeID);
					} else if (key == 3) {
						System.out.println("enter ID ");
						employeeID = mainClass.validate(SCANNER.nextLine());
						System.out.println("enter designation ");
						String newDesignation = mainClass.validateNullString(SCANNER.nextLine());
						director.promoteEmployee(employeeID, newDesignation);

					}else if(key == 4) {
						System.out.println("enter your emp ID ");
						mentorID = mainClass.validate(SCANNER.nextLine());
						director.getEmployeeUnderMentor(mentorID);
					}

					else if (key == 100)
						break;
					else {
						System.out.println("enter valid choice");
						continue;
					}
				}
			} else if (userChoice == 3) {
				while (key != 100) {

					HumanResource humanResource = new HumanResource();
					System.out.println(
							"enter 1 to hire new manger \n enter 2 to delete\n enter 3 to promote employee\nenter 4 to hire fresher\nenter 5 to show employee under you\nenter 100 to go back");
					key = mainClass.validate(SCANNER.nextLine());
					if (key == 1) {
						
						System.out.println("enter department");
						department = mainClass.validateNullString(SCANNER.nextLine());
						humanResource.hireManager(department);
					}

					else if (key == 2) {
						System.out.println("enter ID ");
						employeeID = mainClass.validate(SCANNER.nextLine());
						humanResource.deleteEmployee(employeeID);
					} else if (key == 3) {
						System.out.println("enter ID ");
						employeeID = mainClass.validate(SCANNER.nextLine());
						System.out.println("enter designation ");
						String newDesignation = mainClass.validateNullString(SCANNER.nextLine());
						humanResource.promoteEmployee(employeeID, newDesignation);
					} else if (key == 4) {
						
						System.out.println("enter department");
						department = SCANNER.nextLine();
						System.out.println("enter designation ");
						String designation = SCANNER.nextLine();
						humanResource.hireFresher(designation, department);
					} else if(key == 5) {
						System.out.println("enter your emp ID ");
						mentorID = mainClass.validate(SCANNER.nextLine());
						humanResource.getEmployeeUnderMentor(mentorID);
					}
					else if (key == 100)
						break;
					else {
						System.out.println("enter valid choice");
						continue;
					}

				}
			}
			if (userChoice == 4) {
				while (key != 100) {

					Manager manager = new Manager();
					System.out.println("enter 1 to delete\nenter 2 to promote employee\nenter 3 to show employee under you\nenter 100 to go back");
					key = SCANNER.nextInt();
					if (key == 1) {
						System.out.println("enter ID ");
						employeeID = mainClass.validate(SCANNER.nextLine());
						manager.deleteEmployee(employeeID);
					} else if (key == 2) {
						System.out.println("enter ID ");
						employeeID = mainClass.validate(SCANNER.nextLine());
						System.out.println("enter designation ");
						String newDesignation = mainClass.validateNullString(SCANNER.nextLine());
						manager.promoteEmployee(employeeID, newDesignation);
					}else if (key == 3) {
						System.out.println("enter your emp ID ");
						mentorID = mainClass.validate(SCANNER.nextLine());
						manager.getEmployeeUnderMentor(mentorID);
					} 
					else if (key == 100)
						break;
					else {
						System.out.println("enter valid choice");
						continue;
					}
				}
			}
			
			jdbc.showAllEmployee();
		}

	

	public int validate(String string) {
		int num = 0;
		try {
			num = Integer.parseInt(string);

		} catch (NumberFormatException e) {
			System.out.println("enter valid input");
			num = validate(SCANNER.nextLine());
		}
		return num;
	}

	public String validateNullString(String string) {
		String str = null;
		if (string.isEmpty()) {
			System.out.println("enter valid input");
			str = validateNullString(SCANNER.nextLine());
		}

		return string;

	}
}
