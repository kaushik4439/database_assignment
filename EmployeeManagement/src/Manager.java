import java.sql.SQLException;

public class Manager extends Employee {

	JDBC jdbc = new JDBC();

	public Manager(int employeeID, String name, int age) {
		super(employeeID, name, age);

	}

	public Manager() {

	}

	public boolean promoteEmployee(int employeeID, String newDesignation) {

		try {
			if (!jdbc.checkEmployeeWithID(employeeID)) {
				System.out.println("no employee with this ID");
				return false;
			}
			String designation = jdbc.getDesignation(employeeID);
			if (newDesignation.equalsIgnoreCase(designation)) {
				System.out.println("Already on same post");
				return false;
			}
			if (designation.equalsIgnoreCase("director") || designation.equalsIgnoreCase("ceo")
					|| designation.equalsIgnoreCase("HR") || designation.equalsIgnoreCase("manager")) {
				System.out.println("Sorry you don't have access to promote this employee");
				return false;
			}

			jdbc.promoteEmployee(employeeID, newDesignation);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

	public boolean deleteEmployee(int employeeID) {

		try {
			if (!jdbc.checkEmployeeWithID(employeeID)) {
				System.out.println("no employee with this ID");
				return false;
			}
			String designation = jdbc.getDesignation(employeeID);
			if (designation.equalsIgnoreCase("ceo") || designation.equalsIgnoreCase("director")
					|| designation.equalsIgnoreCase("HR") || designation.equalsIgnoreCase("manager")) {
				System.out.println("Sorry you can't fire this employee");
				return false;
			}
			jdbc.deleteEmployee(employeeID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

}
